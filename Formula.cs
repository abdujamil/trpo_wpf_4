﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TRPO_Lab4.WPF
{
    public static class Formula
    {
        public static double Perimeter { get; set; }
        public static double Apothem { get; set; }
        //Переменные для записи результата рассчетов
        public static double Result { get; set; }

        public static double Decision(double perimeter, double apothem)
        {
            return (1 / 2) * perimeter * apothem; 
        }
        public static void Solve()
        {
           Result  = Decision(Perimeter, Apothem);
        }
    }
}
