﻿using System;
using System.Windows;
using TRPO_Lab4.WPF;

namespace TRPO_Lab4.WPF
{ 
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
        }


        private void Result_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Formula.Perimeter = Convert.ToDouble(txt_Perimetr.Text);
                Formula.Apothem = Convert.ToDouble(txt_Apothem.Text);
            }
            catch(FormatException)
            {
                MessageBox.Show("Неверно введено число.");
            }
            Formula.Solve();
            txt_Result.Text = Convert.ToString(Formula.Result);
                     
        }
    }
}
